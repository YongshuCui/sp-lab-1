# Employee Management System#


### Bitbucket Link ###


* [Link](https://YongshuCui@bitbucket.org/YongshuCui/sp-lab-1.git)

### This system implement an employee database program in C programming. And there are three steps below ###

* Firstly, store employee information in an array of employee structs.
* Secondly, sorted by employee ID value.
* In the last, read in employee data from an input file when it starts-up.


### This program contains 5 important features ###

* Print the Database
* Lookup employee by ID
* Lookup employee by last name
* Add an Employee
* Quit


### Step to run ###

* Open the program directory, The project contains the following file structure:

```
.
├── main.c             // main program 
├── readfile.h         // library about read file, read int, read char, read float 
├── README.md          // program documentation
├── small.txt          // db file
└── a.out              // execute program
```

* Execute program enter small.txt
