#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile.h"

int main(int argc, char *argv[]){
    if (argc <2){
        printf("Please input file name:\n");
        return 0;
    }
    char *fname = argv[1],name[MAXSIZE],lastname[MAXSIZE];
    struct Employee employee[MAXSIZE];
	int option;
	int six_digit_ID;
	int success;
	int salary;
	int yes_or_no;

	int emp_num=0,list[MAXARR];
	if (open_file(fname)==-1){
		printf("error reading file\n");
		return -1;
	}
    
    FILE *fp=fopen(fname,"r");

	while(!feof(fp)){
        fscanf(fp,"%s  %s  %d  %d \n",employee[emp_num].name,employee[emp_num].lastname,&employee[emp_num].salary,&employee[emp_num].id);
		list[emp_num]=employee[emp_num].id;
		emp_num++;
	}
    
	fclose(fp);
	/*
		build a menu
		supply some choice ,each choice has its own function
		loop the menu, unless it choice quit function in menu.
	*/
	while(option!=5){
		printf("\nEmployee DB Menu:\n");
		printf("********************************************************\n");
		printf("(1) Print the Database\n");
		printf("(2) Lookup by ID\n");
		printf("(3) Lookup by Last Name\n");
		printf("(4) Add an Employee\n");
		printf("(5) Quit\n");
		printf("********************************************************\n");
		printf("Enter your choice: ");
		read_int(&option);
		//pemp=employee;
		qsort(employee,emp_num,sizeof(employee[0]),comp);
		//id_sort(pemp,emp_num);
			for(int i=0;i<emp_num;i++){
		 	list[i]=employee[i].id;
			}

		switch(option){
		case 1:
			printf("********************************************************\n");
			printf("Name                     SALARY       ID\n");
			printf("********************************************************\n");
			int i=0;
			while(i!=emp_num){
				printf("%-10s %-10s %10d %10d \n",employee[i].name,employee[i].lastname,employee[i].salary,employee[i].id);
				i++;
			}
			printf("*********************************************************\n");
			printf("Number of Employees (%d)\n",emp_num);
			break;
		case 2:
			printf("Enter a 6 digit employee id: ");
			read_int(&six_digit_ID);
			success =binary_search(list,0,emp_num,six_digit_ID);
			if(success==-1){
				printf("Employee with id %d not found in DB\n",six_digit_ID);
			}else{
				print_by_success(employee,success);
			}
			break;


		case 3:
			printf("Enter Employee's last name (no extra spaces): ");
			read_string(lastname);
			success = search_lastname(employee,emp_num,lastname);
			if(success==-1){
				printf("Employee with lastname %s not found in DB\n",lastname);
			}else{
				print_by_success(employee,success);
			}
			break;
		case 4:
			printf("Enter the first name of the employee: ");
			read_string(name);
			printf("Enter the last name of the employee: ");
			read_string(lastname);
			printf("%s",lastname);
			printf("Enter employee's salary ($30,000 and $150,000): ");
			read_int(&salary);
			printf("do you want to add the following employee to the DB?\n");
			printf("%s %s , salary: %d\n",name,lastname,salary);
			printf("Enter 1 for yes, 0 for no: ");
			read_int(&yes_or_no);
			if(yes_or_no==1){
				if(salary>=30000&&salary<=150000){
					int now_id=employee[emp_num-1].id;
					int new_id=now_id+1;
					if(new_id<100000||new_id>999999){
						printf("id number is out of bound\n");
						break;
					}
					strcpy(employee[emp_num].name,name);
					strcpy(employee[emp_num].lastname,lastname);
					employee[emp_num].salary=salary;
					employee[emp_num].id=new_id;
					list[emp_num]=employee[emp_num].id;
					emp_num++;
					printf("adding successfully.\n");
				}else{
					printf("salary invalid, please enter again.\n");
				}
			}
			break;

		case 5:
			printf("goodbye!\n");
			break;
		default:
			printf("Please input the number of 1-5\n");
			break;
		}
	}
}
